<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    function form(){
        return view('form');
    }

    function welcome(Request $r){
        $name = $r->firstName . " " . $r->lastName;
        return view('welcomee')->with('name', $name);
    }
}
