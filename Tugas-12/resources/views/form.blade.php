<!DOCTYPE html>
<html>

<head>
    <title>Account Baru</title>
</head>

<body>
    <h1>Buat Account Baru</h1>
    <h2>Sign up Form</h2>
    <Form action="/welcome" method="post">
        @csrf
        <div>
            <label for="first-name">First Name</label><br>
            <input id="first-name" name="firstName" type="text" required />
        </div><br>
        <div>
            <label for="last-name">Last Name</label><br>
            <input id="last-name" name="lastName" type="text" required />
        </div><br>
        <div>
            <label>Gender : </label><br>
            <input type="radio" id="male" name="gender" value="Male">
            <label for="male">Male</label><br>
            <input type="radio" id="female" name="gender" value="Female">
            <label for="female">Female</label><br>
            <input type="radio" id="other-gender" name="gender" value="Other">
            <label for="other-gender">Other</label>
        </div><br>
        <div>
            <label>Nationality</label><br>
            <select>
                <option value="Indonesia">Indonesia</option>
                <option value="Inggris">Inggris</option>
            </select>
        </div><br>
        <div>
            <input type="checkbox" id="bahasa" name="bahasa" value="Bahasa Indonesia">
            <label for="bahasa">Bahasa Indonesia</label><br>
            <input type="checkbox" id="english" name="english" value="English">
            <label for="english">English</label><br>
            <input type="checkbox" id="other-lang" name="other-lang" value="Other">
            <label for="other-lang">Other</label><br>
        </div><br>
        <div>
            <label for="bio">Bio :</label><br>
            <textarea id="bio" rows="4"></textarea>
        </div><br>
        <div>
            <button type="submit">Sign Up</button>
        </div>
    </Form>
</body>

</html>