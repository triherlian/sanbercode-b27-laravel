<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    function index(){
        return view('dashboard');
    }
    
    function table(){
        return view('table');
    }

    function datatable(){
        return view('datatable');
    }
}
